import React, { useEffect, useState } from 'react';

function ConferenceForm() {
  const [conferences, setConferences] = useState([]);
  const [name, setName] = useState('');
  const [starts, setStarts] = useState('');
  const [ends, setEnds] = useState('');
  const [description, setDescription] = useState('');
  const [maximum_presentations, setMaximum_Presentations] = useState('');
  const [maximum_attendees, setMaximum_Attendees] = useState('');
  const [conference, setConference] = useState('');

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.name = name;
    data.starts = starts;
    data.ends = ends;
    data.description = description;
    data.maximum_presentations = maximum_presentations;
    data.maximum_attendees = maximum_attendees;
    data.conference = conference;
    console.log(data);

    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);

      setName('');
      setStarts('');
      setEnds('');
      setDescription('');
      setMaximum_Presentations('');
      setMaximum_Attendees('');
      setConference('');
    }
  }

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }
  const handleStartsChange = (event) => {
    const value = event.target.value;
    setStarts(value);
  }

  const handleEndsChange = (event) => {
    const value = event.target.value;
    setEnds(value);
  }

  const handleDescriptionChange = (event) => {
    const value = event.target.value;
    setDescription(value);
  }

  const handleMaximum_PresentationsChange = (event) => {
    const value = event.target.value;
    setMaximum_Presentations(value);
  }

  const handleMaximum_AttendeesChange = (event) => {
    const value = event.target.value;
    setMaximum_Attendees(value);
  }

  const handleConferenceChange = (event) => {
    const value = event.target.value;
    setConference(value);
  }



  const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);


    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (

    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleStartsChange} value={starts} placeholder="Starts" required type="" name="starts" id="starts" className="form-control" />
              <label htmlFor="starts">Starts</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleEndsChange} value={ends} placeholder="Ends" required type="number" name="ends" id="ends" className="form-control" />
              <label htmlFor="ends">Ends</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleDescriptionChange} value={description} placeholder="Description" required type="text" name="description" id="description" className="form-control" />
              <label htmlFor="description">Description</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleMaximum_PresentationsChange} value={maximum_presentations} placeholder="Maximum_Presentations" required type="number" name="maximum_presentations" id="maximum_presentations" className="form-control" />
              <label htmlFor="maximum_presentations">Maximum Presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleMaximum_AttendeesChange} value={maximum_attendees} placeholder="Maximum_Attendees" required type="number" name="maximum_attendees" id="maximum_attendees" className="form-control" />
              <label htmlFor="maximum_attendees">Maximum Attendees</label>
            </div>
            <div className="mb-3">
              <select required name="conference" id="conference" className="form-select" onChange={handleConferenceChange} value={conference}>
            <option value="">Choose a location</option>
                {conferences.map(conference => {
                    return (
                    <option key= {conference.name} value={conference.name}>
                        {conference.name}
                    </option>
                    );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ConferenceForm;
